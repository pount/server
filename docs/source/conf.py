# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
from django.conf import settings
from django import setup
from os.path import abspath, dirname
import sys

DOCUMENTATION_SOURCES = dirname(abspath(__file__))
PROJECT_ROOT = dirname(dirname(DOCUMENTATION_SOURCES))
PROJECT_SOURCES = PROJECT_ROOT
sys.path.insert(0, PROJECT_ROOT)

settings.configure(
    INSTALLED_APPS=[
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'apps.api',
        'apps.search',
    ]
)
setup()  # Setup Django with these settings

# -- Project information -----------------------------------------------------

project = 'Pount'
copyright = '2020, Régis Witz'
author = 'Régis Witz'

# The full version, including alpha/beta/rc tags
release = '0.0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
  'sphinx.ext.autodoc',
  'sphinxcontrib.apidoc',  # automatically call sphinx-apidoc
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html_logo = 'assets/logo.jpg'
html_theme_options = {
    'logo_only': True,
    'prev_next_buttons_location': 'both',
    'style_external_links': True,
}
html_js_files = ['js/customize_sidebar.js', ]
html_css_files = ['css/customize_sidebar.css', ]


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


#########################
# sphinx-contrib.apidoc #
#########################
# sphinx-apidoc is a tool for automatic generation of sphinx documentation
# (ie. .rst files) from docstrings present in source files.
# However, normally this requires to manually call sphinx-apidoc each time,
# before calling sphinx-build (ie. make html).
# Extension sphinx-contrib.apidoc eliminates this need, and makes documentation
# generation part of the documentation build. Especially useful for RTD.
# See: https://github.com/sphinx-contrib/apidoc
apidoc_module_dir = PROJECT_ROOT
apidoc_output_dir = DOCUMENTATION_SOURCES
apidoc_excluded_paths = [
        '*/tests', 'run_tests.py',
        'manage.py', 'setup.py',
        ]
apidoc_separate_modules = True
