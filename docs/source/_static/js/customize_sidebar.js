
code = `
<div class='sidebartext'>
  <p>Pount is an elegant, simple and free software to help you structure, visualize and valorize your data.</p>
  <p>Sponsored by the <em><a href='https://www.unistra.fr'>University of Strasbourg</a></em>.</p>
  <h3>Useful links:</h3>
  <ul>
    <li><a href='https://gitlab.com/pount/client'>Pount client repository</a></li>
    <li><a href='https://gitlab.com/pount/server'>Pount server repository</a></li>
    <li><a href='https://docs.djangoproject.com'>Django documentation</a></li>
    <li><a href='https://www.django-rest-framework.org/'>Django REST framework</a></li>
    <li><a href='https://vuejs.org/v2/guide/'>Vue.js documentation</a></li>
    <li><a href='https://vuetifyjs.com'>Vuetify documentation</a></li>
  </ul>
</div>
`

const callback = function (mutations, observer) {
  let elements = document.getElementsByClassName('wy-menu');
  if (elements.length > 0) {
    let menu = elements[0];
    menu.insertAdjacentHTML('afterbegin', code);
    observer.disconnect();
    return;
  }
};
const observer = new MutationObserver(callback);
observer.observe(document, { childList: true, subtree: true, });
