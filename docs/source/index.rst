.. Pount documentation master file, created by
   sphinx-quickstart on Sun May  3 15:00:58 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pount: Scientific data under the spotlight!
===========================================

.. image:: https://img.shields.io/badge/license-AGPL3.0-informational?logo=gnu&color=important
    :target: https://www.gnu.org/licenses/agpl-3.0.html

.. image:: https://img.shields.io/badge/python-3.8-informational
    :target: https://pount.gitlab.io/

*Pount* is an elegant, simple and free software to help you structure, visualize and valorize your data.

-------------------

What am I reading?
------------------

Documentation for project **Pount**, which is available on `Read the Docs <https://pount.readthedocs.io/en/latest/>`_.

What is Pount?
--------------

Scientific work is much more than just publications.
Indeed, unpublished failures and experiments constitute a real wealth of untapped knowledge.
Thus, to preserve and spread these research data helps to argue success, remember mistakes and reveal unexplored ideas.
To store, reference and expose these data is a proof of sustainability and traceability, on which the scientific approach is based on, such as experiments reproducibility and extensibility.

In the open science context, **Pount** is a free, modular and interoperable ecosystem for accessing knowledge

Why should I use it?
--------------------

Pount is ready for the needs of today's researchers.

- save and version your data (documents, images, videos, 3D models) ;
- structure your data according to your scientific field standards, and extend these standards with your very own custom metadata ;
- share your data with a unique identifier, a citation template, and set up dedicated permissions to your communities ;
- visualize your data with a fast and flawless rendering, and enhance them with contextual information and hyperlinks.

Need a long-term storage for your data?
Check `Zenodo <https://zenodo.org/>`_, `Dataverse <https://dataverse.org/>`_ or `Nakala <https://www.nakala.fr/>`_.

Are there any related repositories?
-----------------------------------
Sources for Pount server / API are hosted `here <https://gitlab.com/pount/client>`_, whereas sources for Pount client / website are hosted `here <https://gitlab.com/pount/server>`_.

How can I test it?
------------------

Just `visit Pount website <https://pount.gitlab.io>`_ to discover its features first-hand.

If you're already convinced and want to deploy your very own instance of Pount, :doc:`this page <dev/setup>` describes how to setup a working environment as a developer.

How can I contribute?
---------------------

If it helps you, just using Pount and telling people who might need it about it is fine.

If you want to be more involved, feel free to :

- submit bugs and feature requests, and to help validate them when they are committed ;
- review the documentation and help make sure everything is helpful and understandable ;
- make merge requests for anything from typos to new features ;
- ... or anything else you have in mind !

What does "Pount" mean?
-----------------------

Pount stands for *Plateforme OUverte Numérique Transdisciplinaire* in French.
Pount is a reference to the `Land of Punt <https://en.wikipedia.org/wiki/Land_of_Punt>`_ (written *Pays de Pount* in French).

Is it any good?
---------------

`Yes <https://news.ycombinator.com/item?id=3067434>`_.

And who are you?
----------------

Pount is supported by the following organisms:

- `Maison Interuniversitaire des Sciences de l'Homme - Alsace (MISHA, UMR7044, USR3227) <https://www.misha.fr/>`_
- `University of Strasbourg <https://www.unistra.fr/>`_, and its `Direction du Numérique (DNUM) <https://services-numeriques.unistra.fr/organisation-du-numerique/direction-du-numerique-dnum.html>`_

Feel free to contact `pount@unistra.fr <mailto:pount@unistra.fr>`_ for any additional information, question or comment.



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dev/contributing
   dev/setup
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
