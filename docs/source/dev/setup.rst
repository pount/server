.. _introduction:

API setup
=========

This document describes the procedure for installing the Pount *server* / API.
The *client* / website installation is described `here <TODO>`_.

Install PostgreSQL
------------------

**Install dependencies**::

    >>> sudo apt install postgresql postgresql-server-dev-10

Change to user ``postgres``, login to postgresql prompt and (if necessary) set password::

    >>> sudo -u postgres -i
    >>> psql
    >>> /password password

**Create database**, and check it is created with::

    >>>  CREATE DATABASE pount-db;
    >>> /l

⚠️ Yeah, *there's an additional space* before ``CREATE DATABASE`` and other SQL commands ...

Alternative::

    >>> psql
    >>>  CREATE USER username;
    >>> /du
    >>> /q
    >>> psql pount-db
    >>>  GRANT ALL PRIVILEGES ON DATABASE pount-db TO username;


Install Elasticsearch
---------------------

From `the manual <https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html>`_, you can chose the ``.tar.gz`` option.
See `this page <https://www.elastic.co/guide/en/elasticsearch/reference/current/targz.html>`_ for additional info.

**Download** sources (283Mo), **check** download is all good, then **extract** them in a subfolder::

    >>> wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.6.2-linux-x86_64.tar.gz
    >>> wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.6.2-linux-x86_64.tar.gz.sha512
    >>> shasum -a 512 -c elasticsearch-7.6.2-linux-x86_64.tar.gz.sha512
    >>> tar -xzf elasticsearch-7.6.2-linux-x86_64.tar.gz
    >>> mv elasticsearch-7.6.2 elasticsearch

**Run Elasticsearch**::

    >>> ./elasticsearch/bin/elasticsearch

To check everything is up and running, you can open a new terminal and run::

    >>> curl -XGET http://localhost:9200

If you get some JSON answer like this one, you should be okay::

   {
     "name" : "something",
     "cluster_name" : "elasticsearch",
     "cluster_uuid" : "something something",
     "version" : {
       "number" : "5.6.16",
       "build_hash" : "something",
       "build_date" : "somewhere in the past",
       "build_snapshot" : false,
       "lucene_version" : "6.6.1"
     },
     "tagline" : "You Know, for Search"
   }

Build Pount API
---------------

**Get the sources**::

    >>> git clone git@gitlab.com:pount/server.git
    >>> cd server

Setup your **virtual environment**::

    >>> virtualenv -p /usr/bin/python3.8 venv
    >>> source venv/bin/activate

You can learn why using a virtual environment is a good idea `here <https://realpython.com/python-virtual-environments-a-primer/>`_, and more about ``venv`` `here <https://docs.python.org/fr/3/library/venv.html>`_.

Alternatively, you can use `virtualenv <https://pypi.org/project/virtualenv/>`_ or `pipenv <https://realpython.com/pipenv-guide/>`_, but ``venv`` is simple, shipped with python and is `now the recommended way to do it <https://docs.python.org/3/library/venv.html>`_, so this documentation will assume you use ``venv``.

Download and **install dependencies**::

    >>> pip install -r requirements/dev.txt

Build and **run unit tests**::

    >>> tox

**Run Pount server**::

    >>> python manage.py makemigrations
    >>> python manage.py migrate
    >>> python manage.py createsuperuser
    >>> python manage.py runserver
