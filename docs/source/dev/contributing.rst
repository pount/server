Contributing
============

🎉 First off, thanks for taking the time to contribute! 🦄👍

The following document is a set of guidelines for contributing to the different
repositories hosted in the `Pount group <https://gitlab.com/pount>`_ on Gitlab.

Remember that Pount purpose is to empower researchers, and make them able to
make the best use of their data.
However, research takes time and, truth is, doesn't have a lot of money.
Thus, regarding the code of its components, Pount main value is *sustainability*.
This include low-cost operation and easy maintenance.

| What will this codebase become in 100 years ?
| Answer is, we have no idea. But research data must remain available nevertheless.
  So by always enforcing lightweight, easy to replace, optional components,
  we think that we will always be able to manage every change that will happen.
| And maybe even welcome it.



Unit testing
------------

Unit test *everything* you implement.
In other words, if you deliver anything that makes the number of statements
uncovered by unit test *decrease*, you compromise the quality of the whole
project. Don't do that. Don't postpone unit tests writing, you won't do it,
and nobody will do it at your place. This is how legacy code is created.
Unit test *now*, while the feature exact scope is still fresh in your mind.

Just to be sure, let's say it again:

* Never, **ever** merge code that decreases code coverage
* Improve code coverage whenever you can

Always push cleaner code that what you pulled.



Git commit messages
-------------------

* Use the present tense: "*Add feature*" not "*Added feature*"
* Use the imperative mood: "*Move cursor to...*" not "*Moves cursor to...*"
* Limit the first line to 72 characters or less
* Reference issues and pull requests liberally after the first line
* Avoid purely aesthetic modifications, such as whitespace commits
* Consider starting the commit message with an applicable
  `gitmoji <https://gitmoji.carloscuesta.me/>`_.
  In particular:
 * ✨ ``:sparkles:`` when introducing new, finished, tested features
 * 🚧 ``:construction:`` when commiting a well scoped requirement
   for a yet unfinished feature
 * ♻️ ``:recycle:`` when refactoring code
 * 🔥 ``:fire:`` when removing code or files
 * ✅ ``:white_check_mark:`` when updating tests, or improving test coverage
 * 🐛 ``:bug:`` when fixing a bug
 * 📝 ``:pencil:`` when writing docs
 * 🌐 ``:globe_with_meridians:`` when translating documentation or UI text
 * ➕ ``:heavy_plus_sign:`` when adding dependencies
 * ⬆️ ``:arrow_up:`` when upgrading dependencies
 * | 🔀 ``:twisted_rightwards_arrows:`` when merging branches
   | *Consider using fast forward commits instead.*
  You can of course use the following kinds of commits.
  However, these should ALWAYS be squashed before being merged
  into ``master`` branch!
 * 💩 ``:poop:`` when introducing untested or unrefactored features
 * 💚 ``:green_heart:`` when fixing the CI build
 * 🚨 ``:rotating_light:`` when removing linter warnings



Coding Styleguide
-----------------

This is open source software.
Consider the people who will read your code, and make it look nice for them.

| All Python code must be compliant with `PEP-8 <https://www.python.org/dev/peps/pep-0008/>`_.
| You can generally check your code compliancy with ``tox -e pep8``.

| All Javascript code is linted with `ESLint <https://eslint.org/>`_.
| You can generally check your code compliancy with ``npm run lint``.



Dependencies management
-----------------------

Project Pount is licensed under the termes of the
`GNU Affero GPL licence <https://www.gnu.org/licenses/agpl-3.0.html>`_.
Thus, make sure you:

* Use only copyleft compatible licences ;
  `here's how <https://www.gnu.org/licenses/license-compatibility.html>`_
  (`here <https://www.gnu.org/licenses/license-compatibility.fr.html>`_ is
  the same in French).
* Use tools to help you double-check licenses of **all** your dependencies.
  Here is an `example on how to do it for Node.js projects
  <https://medium.com/@fokusman/the-easiest-way-to-check-all-your-npm-dependency-licenses-753075ef1d9d>`_.
  For Python projects, you can use `pkg_resources
  <https://setuptools.readthedocs.io/en/latest/pkg_resources.html>`_.
  This is valid for indirect dependencies, too!
* When you add a new dependency to a Pount project, *always document why*.
  Explain why you didn't choose the alternatives, and if there are any version
  issues.
  These explanations should always live at the same place as the dependencies.
  For Python projects, this means in the ``requirements*.txt`` files.
  For Node.js projects, this means in the ``package.json`` file.
  Talking about dependency listing files, you're responsible of them:
  don't let your IDE or builder or what have you modify them without noticing
  or even understanding what happens, and don't let their formatting be spoiled.

Remember that Pount repositories are meant to be usable by anyone, with the least possible hassle.
Thus, keep modifications that are specific to you or your organization separated
from the default repositories.
These includes specific settings or urls, credentials or access methods,
deployment scripts, Django🦄 migrations, and so on.
Of course, unless otherwise stated you are free to fork our repositories
to add those specifics, as long as you remain within the terms of the applicable license.



Documentation
-------------

| Code documentation is generated by `Sphinx <https://www.sphinx-doc.org>`_,
  using `reStructured Text syntax
  <https://pythonhosted.org/an_example_pypi_project/sphinx.html>`_.
| Do not use Markdown for Pount documentation.
  `Here are some of the whys <https://www.adamhyde.net/whats-wrong-with-markdown/>`_.

Language of code documentation and user documentation is English.

Document your code in a way that is the most helpful for your fellow developpers:

* If a class or function purpose and behaviour is not immediately understandable
  by reading its profile, by all means, document it.
* Always document external API functions.
* There's no need to rephrase what the function or class profile already says.
  You generally don't need to explain *what* happens ;
  instead, explain *why*, and *how* it happens.
  Add *details*, add *context*, add possible *issues*.
 * Why is this necessary?
 * Why does it work like it does, and not more trivially?
 * What is the *purpose* of it?
   Consider providing a link to the specification of the concerned feature,
   and/or to the issue signaling the defect (especially in an external library).
 * Are there some caveats or unspecified cases concerning its implementation?
   Be exhaustive documenting them. And, while you're at it, **unit test** them!
 * How should it be used by the caller?
   Consider providing examples!
 * Are there some brittle or difficult to understand parts in the implementation?
   If so, consider inserting comment into the body of the class/function
   to help update/fix tricky parts.

If you postpone code commenting, you won't do it.
So do it. And do it *now*.
