Pount: Scientific data under the spotlight!
===========================================

.. image:: https://img.shields.io/badge/license-AGPL3.0-informational?logo=gnu&color=important
    :target: https://www.gnu.org/licenses/agpl-3.0.html

.. image:: https://img.shields.io/badge/python-3.8-informational
    :target: https://pount.gitlab.io/

*Pount* is an elegant, simple and free software to help you structure, visualize and valorize your data.

Please check project documentation available on `Read the Docs <https://pount.readthedocs.io/en/latest/>`_.
