import rules


@rules.predicate
def is_user_creator(user):
    return user.is_creator


@rules.predicate
def is_item_public(user, item):
    set_public = (item.set is None) or (is_set_public(user, item.set))
    return set_public and item.is_public


@rules.predicate
def is_item_owner(user, item):
    return item.owner == user


@rules.predicate
def is_item_set_owner(user, item):
    return item.set and item.set.owner == user


@rules.predicate
def is_item_set_member(user, item):
    return item.set and is_set_member(user, item.set)


@rules.predicate
def is_item_set_contributor(user, item):
    return item.set and is_set_contributor(user, item.set)


@rules.predicate
def is_set_public(user, set):
    return set.is_public


@rules.predicate
def is_set_owner(user, set):
    return set.owner == user


@rules.predicate
def is_set_member(user, set):
    return set.get_role(user) is not None


@rules.predicate
def is_set_contributor(user, set):
    role = set.get_role(user)
    return role == 'owner' or role == 'contributor'


SET_CREATE = 'set.create'
SET_DELETE = 'set.delete'
SET_VIEW = 'set.view'
SET_EDIT = 'set.edit'
# TODO specializations of SET_EDIT
# SET_TEMPLATE = 'set.template'
# SET_SETTINGS = 'set.settings'  # per settings would be nice
# SET_USER_ADD = 'set.users.add'
# SET_USER_KICK = 'set.users.kick'
# SET_USER_ROLE = 'set.users.role'
# SET_ITEM_CREATE = 'set.item.create'
# SET_ITEM_DELETE = 'set.item.delete'  # per item would be nice
# SET_ITEM_EDIT = 'set.item.edit'  # per item would be nice

ITEM_CREATE = 'item.create'
ITEM_DELETE = 'item.delete'
ITEM_VIEW = 'item.view'  # if in set, per item would ne nice
ITEM_EDIT = 'item.edit'  # if in set, per item would ne nice
# TODO specializations of ITEM_EDIT if Item is in a set
# ITEM_METADATA = 'items.metadata'  # per item | per metadata wbnice
# ITEM_SETTINGS = 'items.settings'  # per item | per setting wbnice


def initialize_rules():

    rules.add_perm(
            ITEM_VIEW,
            is_item_public | is_item_owner | is_item_set_member
            )
    rules.add_perm(
            ITEM_EDIT,
            is_item_owner | is_item_set_contributor
            )
    rules.add_perm(
            ITEM_CREATE,
            is_user_creator
            )
    rules.add_perm(
            ITEM_DELETE,
            is_item_owner | is_item_set_owner
            )

    rules.add_perm(
            SET_VIEW,
            is_set_public | is_set_owner | is_set_member
            )
    rules.add_perm(
            SET_EDIT,
            is_set_owner | is_set_contributor
            )
    rules.add_perm(
            SET_CREATE,
            is_user_creator
            )
    rules.add_perm(
            SET_DELETE,
            is_set_owner
            )
