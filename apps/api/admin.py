# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import (
    Item, Set, Template,
    User, Group, Role, Permission,
)

admin.site.register(Item)
admin.site.register(Set)
admin.site.register(Template)
admin.site.register(User, UserAdmin)
admin.site.register(Group)
admin.site.register(Role)
admin.site.register(Permission)
