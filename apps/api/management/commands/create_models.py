# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from apps.api.models import init


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        init.initialize_roles()
        init.initialize_templates()
