# -*- coding: utf-8 -*-
from rest_framework import viewsets, mixins
from ..models import Role, Permission
from ..serializers import RoleSerializer, PermissionSerializer
from ..pagination import StandardPagination


class RoleViewSet(mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    """
    API endpoint that allows roles to be viewed, but not edited.
    """
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    pagination_class = StandardPagination


class PermissionViewSet(mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):
    """
    API endpoint that allows specific permissions to be retrieved,
    but nothing else (no create, no update, no delete).
    """
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer
    pagination_class = StandardPagination
