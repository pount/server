# -*- coding: utf-8 -*-
from rest_framework import viewsets, mixins
from ..models import Template
from ..serializers import TemplateSerializer
from ..pagination import StandardPagination


class TemplateViewSet(mixins.RetrieveModelMixin,
                      mixins.ListModelMixin,
                      viewsets.GenericViewSet):
    """
    API endpoint that allows templates to be viewed, but not edited.
    """
    queryset = Template.objects.all()
    serializer_class = TemplateSerializer
    pagination_class = StandardPagination
