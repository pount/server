# -*- coding: utf-8 -*-
from rest_framework import viewsets
from ..models import Set
from ..serializers import SetSerializer
from ..pagination import StandardPagination


class SetViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows sets to be viewed or edited.
    """
    queryset = Set.objects.all()
    serializer_class = SetSerializer
    pagination_class = StandardPagination
