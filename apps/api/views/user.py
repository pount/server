# -*- coding: utf-8 -*-
from rest_framework import viewsets, mixins
from ..serializers import UserSerializer
from ..pagination import StandardPagination
from django.contrib.auth import get_user_model
User = get_user_model()


class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    """
    API endpoint that allows users to be viewed, but not edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = StandardPagination
