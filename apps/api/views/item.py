# -*- coding: utf-8 -*-
from rest_framework import viewsets
from ..models import Item
from ..serializers import ItemSerializer
from ..pagination import StandardPagination


class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows items to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    pagination_class = StandardPagination

    def perform_create(self, serializer):
        settings = serializer.validated_data['settings']
        source = settings.get('source', {})
        if source:
            service = source['service']
            if service == 'zenodo':
                data = serializer.zenodo_response.json()
                serializer.validated_data['metadata'] = data['metadata']
                # serializer.validated_data['files'] = data['files']
        return super(ItemViewSet, self).perform_create(serializer)
