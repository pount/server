# -*- coding: utf-8 -*-
from rest_framework import pagination


class StandardPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000


class CursorPagination(pagination.CursorPagination):
    """
    When using PagePagination, if user asks for page N, then an item is added
    somehow, and then user asks for page N+1, the last item of page N can be
    shown again as the first item in page N+1.
    This doesn't happen with CursorPagination, which keeps references to
    objects, and does not have to calculate content for each page.
    """
    page_size = 10
    page_size_query_param = 'page_size'
    # ordering = '-creation'  # default
    ordering = '-updated'
