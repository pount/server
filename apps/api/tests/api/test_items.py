# -*- coding: utf-8 -*-
from django.urls import reverse
from rest_framework.views import status
import json
from .common import ViewTestCase
from ...models import Item


class ItemList(ViewTestCase):

    def test_empty_list(self):
        r = self.client.get(reverse('item-list'))
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(0, r.data['count'])
        self.assertEqual([], r.data['results'])
        self.assertIsNone(r.data['previous'])
        self.assertIsNone(r.data['next'])


class ItemCreateFailuresBecauseOfSettingsSource(ViewTestCase):

    def test_create_fails_if_empty(self):
        r = self.client.post(reverse('item-list'), {})
        content = json.loads(r.content)
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(1, len(content['metadata']))
        self.assertTrue('required' in content['metadata'][0])
        self.assertEqual(1, len(content['settings']))
        self.assertTrue('required' in content['settings'][0])
        self.assertEqual(1, len(content['owner']))
        self.assertTrue('required' in content['owner'][0])
        self.assertEqual(0, Item.objects.count())

    def test_create_fails_if_source_missing(self):
        data = {'settings': {}, }
        response = self.client.post(reverse('item-list'), data, format='json')
        error_message = self.assert400(response, 'settings')[0]
        self.assertTrue('source' in error_message)
        self.assertTrue('required' in error_message)

    def test_create_fails_if_source_empty(self):
        data = {'settings': {'source': {}, }, }
        response = self.client.post(reverse('item-list'), data, format='json')
        error_messages = self.assert400(response, 'settings', error_count=2)
        self.assertTrue('source.url' in error_messages[0])
        self.assertTrue('required' in error_messages[0])
        self.assertTrue('source.service' in error_messages[1])
        self.assertTrue('one of' in error_messages[1])

    def test_create_fails_if_missing_fields_in_source(self):
        data = {
                'settings': {
                    'source': {
                        'whatever': 'whatever',
                        },
                    },
                }
        response = self.client.post(reverse('item-list'), data, format='json')
        error_messages = self.assert400(response, 'settings', error_count=2)
        self.assertTrue('source.url' in error_messages[0])
        self.assertTrue('required' in error_messages[0])
        self.assertTrue('source.service' in error_messages[1])
        self.assertTrue('one of' in error_messages[1])

    def test_create_fails_if_missing_service_in_source(self):
        data = {
                'settings': {
                    'source': {
                        'url': 'http://example.com',
                        },
                    },
                }
        response = self.client.post(reverse('item-list'), data, format='json')
        error_message = self.assert400(response, 'settings')[0]
        self.assertTrue('source.service' in error_message)
        self.assertTrue('one of' in error_message)

    def test_create_fails_if_invalid_service_in_source(self):
        data = {
                'settings': {
                    'source': {
                        'service': 'whatever',
                        'url': 'http://example.com',
                        },
                    },
                }
        response = self.client.post(reverse('item-list'), data, format='json')
        error_message = self.assert400(response, 'settings')[0]
        self.assertTrue('source.service' in error_message)
        self.assertTrue('one of' in error_message)

    def test_create_fails_if_missing_url_in_source(self):
        data = {
                'settings': {
                    'source': {
                        'service': 'zenodo',
                        },
                    },
                }
        response = self.client.post(reverse('item-list'), data, format='json')
        error_message = self.assert400(response, 'settings')[0]
        self.assertTrue('source.url' in error_message)
        self.assertTrue('required' in error_message)

    def test_create_fails_if_invalid_url_in_source(self):
        data = {
                'settings': {
                    'source': {
                        'service': 'zenodo',
                        'url': 'whatever',
                        },
                    },
                }
        response = self.client.post(reverse('item-list'), data, format='json')
        error_message = self.assert400(response, 'settings')[0]
        self.assertTrue('source.url' in error_message)
        self.assertTrue('valid' in error_message)
