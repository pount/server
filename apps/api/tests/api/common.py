# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from rest_framework import test, routers
from rest_framework.views import status
import json
from ... import views


def create_router():
    router = routers.DefaultRouter()
    router.register(r'items', views.ItemViewSet)
    router.register(r'templates', views.TemplateViewSet)
    router.register(r'users', views.UserViewSet)
    router.register(r'roles', views.RoleViewSet)
    return router


class ViewTestCase(test.APITestCase, test.URLPatternsTestCase):
    # TIP: one can see available routes using:
    # from django.urls import get_resolver
    # print(set(k for k, v in get_resolver(None).reverse_dict.items()))
    urlpatterns = [url(r'^api/', include(create_router().urls)), ]

    def assert400(self, r, key, error_count=1):
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
        content = json.loads(r.content)
        error_messages = content[key]
        self.assertEqual(error_count, len(error_messages), error_messages)
        return error_messages
