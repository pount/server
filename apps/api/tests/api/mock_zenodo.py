# -*- coding: utf-8 -*-

# This can be used to mock requests.get
def requests_get(*args, **kwargs):

    class Response:

        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    if args[0] == 'https://zenodo.org/api/records/42':
        return Response({
            'metadata': {'title': 'test'},
            'files': [],
            },
            200)
        """
    elif args[0] == 'http://dataverse.org/api/datasets/42':
        return Response({
            'data': {
                'latestVersion': {
                    'metadataBlocks': {
                        'citation': {
                            'displayName': "Citation Metadata",
                            'fields': [
                                {
                                    'typeName': 'title',
                                    'multiple': False,
                                    'typeClass': 'primitive',
                                    'value': 'test',
                                    },
                                ],
                            }
                        }
                    }
                }
            },
            200)
        """
    return Response(None, 404)
