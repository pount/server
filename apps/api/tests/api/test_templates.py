# -*- coding: utf-8 -*-
from django.urls import reverse
from rest_framework.views import status
from .common import ViewTestCase
from ...models import Template, init


class TemplateTestCase(ViewTestCase):
    def setUp(self):
        init.initialize_templates()

    def tearDown(self):
        Template.objects.all().delete()


class ListTemplatesTest(TemplateTestCase):

    def test_empty_list(self):
        r = self.client.get(reverse('template-list'))
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(2, r.data['count'])
        self.assertIsNone(r.data['previous'])
        self.assertIsNone(r.data['next'])
        results = r.data['results']
        self.assertEqual(2, len(results))
        self.assertEqual('minimal', results[0]['settings']['name'])
        self.assertEqual('datacite', results[1]['settings']['name'])


class CreateTemplateTest(TemplateTestCase):

    def test_post_failure(self):
        '''
        Disable user template creation for now.
        '''
        data = {
                'metadata': {'title': 'text', 'mandatory': True, },
                'settings': {'name': 'test', },
                }
        r = self.client.post(reverse('template-list'), data, format='json')
        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class UpdateTemplateTest(TemplateTestCase):

    def test_put_failure(self):
        '''
        Disable user template update for now.
        '''
        data = {
                'metadata': {'title': 'text', 'mandatory': True, },
                'settings': {'name': 'test', },
                }
        url = reverse('template-detail', kwargs={'pk': 1})
        r = self.client.put(url, data, format='json')
        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class DestroyTemplateTest(TemplateTestCase):

    def test_delete_failure(self):
        '''
        Disable user template deletion for now.
        '''
        x = self.client.get(reverse('template-list')).data['results'][0]['url']
        pk = int(x.split('/')[-2])  # template url ends with /templates/5/
        url = reverse('template-detail', kwargs={'pk': pk})
        r = self.client.delete(url)
        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
