# -*- coding: utf-8 -*-
from django.urls import reverse
from rest_framework.views import status
from .common import ViewTestCase
from unittest import mock
from .mock_zenodo import requests_get


class ItemCreateFromZenodo(ViewTestCase):

    def setUp(self):
        from ..util import create_user
        self.user = create_user('jane.doe', True)
        # you can log in like this, but you'll only get AnonymousUser:
        # self.client.login(username='jane.doe', password='password')
        self.client.force_authenticate(self.user)
        # now this is how you serialize an user for PUT requests
        # remember, we use HyperLinkedModelSerializers, so this is needed
        from django.test.client import RequestFactory
        from django.urls import reverse
        from ...serializers import UserSerializer
        context = {'request': RequestFactory().get(reverse('user-list'))}
        self.owner = UserSerializer(self.user, context=context)

    def tearDown(self):
        self.user.delete()

    @mock.patch(
            'apps.api.importers.zenodo.requests.get',
            side_effect=requests_get
            )
    def test_create_item_from_zenodo(self, mock_get):
        url = 'https://zenodo.org/api/records/42'
        data = {
                'metadata': {},
                'settings': {
                    'source': {
                        'service': 'zenodo',
                        'url': url,
                        },
                    },
                'owner': self.owner.data['url'],
                }
        response = self.client.post(reverse('item-list'), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn(mock.call(url), mock_get.call_args_list)
        self.assertEqual(len(mock_get.call_args_list), 1)

    @mock.patch(
            'apps.api.importers.zenodo.requests.get',
            side_effect=requests_get
            )
    def test_create_failure_if_item_not_found_on_zenodo(self, mock_get):
        data = {
                'metadata': {},
                'settings': {
                    'source': {
                        'service': 'zenodo',
                        'url': 'https://zenodo.org/api/records/99',
                        },
                    },
                'owner': self.owner.data['url'],
                }
        response = self.client.post(reverse('item-list'), data, format='json')
        # we received a 404 from Zenodo, BUT it's because of a bad request
        # (wrong url as a parameter) ...
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @mock.patch(
            'apps.api.importers.zenodo.requests.get',
            side_effect=requests_get
            )
    def test_create_failure_if_wrong_importer_name(self, mock_get):
        data = {
                'metadata': {},
                'settings': {
                    'source': {
                        'service': 'whatever',
                        'url': 'https://zenodo.org/api/records/99',
                        },
                    },
                'owner': self.owner.data['url'],
                }
        response = self.client.post(reverse('item-list'), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
