# -*- coding: utf-8 -*-
from django.urls import reverse
from rest_framework.views import status
from .common import ViewTestCase
from ...models import Role, init


class RoleTestCase(ViewTestCase):
    def setUp(self):
        init.initialize_roles()

    def tearDown(self):
        Role.objects.all().delete()


class ListRolesTest(RoleTestCase):

    def test_empty_list(self):
        r = self.client.get(reverse('role-list'))
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(3, r.data['count'])
        self.assertIsNone(r.data['previous'])
        self.assertIsNone(r.data['next'])
        results = r.data['results']
        self.assertEqual(3, len(results))
        self.assertEqual('owner', results[0]['name'])
        self.assertEqual('contributor', results[1]['name'])
        self.assertEqual('member', results[2]['name'])


class CreateRoleTest(RoleTestCase):

    def test_post_failure(self):
        '''
        Disable role creation for now.
        '''
        r = self.client.post(reverse('role-list'), {}, format='json')
        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class UpdateRoleTest(RoleTestCase):

    def test_put_failure(self):
        '''
        Disable role update for now.
        '''
        url = reverse('role-detail', kwargs={'pk': 1})
        r = self.client.put(url, {}, format='json')
        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class DestroyRoleTest(RoleTestCase):

    def test_delete_failure(self):
        '''
        Disable user role deletion for now.
        '''
        x = self.client.get(reverse('role-list')).data['results'][0]['url']
        pk = int(x.split('/')[-2])  # role url ends with /roles/5/
        url = reverse('role-detail', kwargs={'pk': pk})
        r = self.client.delete(url)
        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
