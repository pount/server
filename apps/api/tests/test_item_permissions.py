# -*- coding: utf-8 -*-
from .test_permissions import PermissionsTestCase
from ..rules import ITEM_VIEW, ITEM_EDIT, ITEM_CREATE, ITEM_DELETE


class CreatorPermissionsTest(PermissionsTestCase):

    def test_creator_can_create_item(self):
        self.assertTrue(self.creator.has_perm(ITEM_CREATE))

    def test_creator_can_view_public_item(self):
        permission = self.creator.has_perm(ITEM_VIEW, self.item_public)
        self.assertEqual(True, permission)

    def test_creator_can_edit_public_item(self):
        permission = self.creator.has_perm(ITEM_EDIT, self.item_public)
        self.assertEqual(True, permission)

    def test_creator_can_delete_public_item(self):
        permission = self.creator.has_perm(ITEM_DELETE, self.item_public)
        self.assertEqual(True, permission)

    def test_creator_can_view_private_item(self):
        permission = self.creator.has_perm(ITEM_VIEW, self.item_private)
        self.assertEqual(True, permission)

    def test_creator_can_edit_private_item(self):
        permission = self.creator.has_perm(ITEM_EDIT, self.item_private)
        self.assertEqual(True, permission)

    def test_creator_can_delete_private_item(self):
        permission = self.creator.has_perm(ITEM_DELETE, self.item_private)
        self.assertEqual(True, permission)


class FriendsPermissionsTest(PermissionsTestCase):

    def test_group_member_can_create_item(self):
        self.assertTrue(self.friends.has_perm(ITEM_CREATE))

    def test_group_member_can_view_public_item(self):
        permission = self.friends.has_perm(ITEM_VIEW, self.item_public)
        self.assertEqual(True, permission)

    def test_group_member_cannot_edit_private_item(self):
        permission = self.friends.has_perm(ITEM_EDIT, self.item_private)
        self.assertEqual(False, permission)

    def test_group_member_cannot_delete_public_item(self):
        permission = self.friends.has_perm(ITEM_DELETE, self.item_public)
        self.assertEqual(False, permission)

    def test_group_member_cannot_view_private_item(self):
        permission = self.friends.has_perm(ITEM_VIEW, self.item_private)
        self.assertEqual(False, permission)

    def test_group_member_cannot_edit_public_item(self):
        permission = self.friends.has_perm(ITEM_EDIT, self.item_public)
        self.assertEqual(False, permission)

    def test_group_member_cannot_delete_private_item(self):
        permission = self.friends.has_perm(ITEM_DELETE, self.item_private)
        self.assertEqual(False, permission)


class VisitorPermissionsTest(PermissionsTestCase):

    def test_visitor_cannot_create_item(self):
        self.assertFalse(self.visitor.has_perm(ITEM_CREATE))

    def test_visitor_can_view_public_item(self):
        permission = self.visitor.has_perm(ITEM_VIEW, self.item_public)
        self.assertEqual(True, permission)

    def test_visitor_cannot_edit_public_item(self):
        permission = self.visitor.has_perm(ITEM_EDIT, self.item_public)
        self.assertEqual(False, permission)

    def test_visitor_cannot_delete_public_item(self):
        permission = self.visitor.has_perm(ITEM_DELETE, self.item_public)
        self.assertEqual(False, permission)

    def test_visitor_cannot_view_private_item(self):
        permission = self.visitor.has_perm(ITEM_VIEW, self.item_private)
        self.assertEqual(False, permission)

    def test_visitor_cannot_edit_private_item(self):
        permission = self.visitor.has_perm(ITEM_EDIT, self.item_private)
        self.assertEqual(False, permission)

    def test_visitor_cannot_delete_private_item(self):
        permission = self.visitor.has_perm(ITEM_DELETE, self.item_private)
        self.assertEqual(False, permission)
