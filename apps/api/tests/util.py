# -*- coding: utf-8 -*-
from ..models import Group, Set, Item
from django.contrib.auth import get_user_model
User = get_user_model()


def create_user(name, is_creator):
    email = '%s@pount.org' % name
    info = {'username': name, 'email': email, }
    settings = {'creator': is_creator}
    return User.objects.create_user(
            username=name,
            email=email,
            info=info,
            settings=settings,
            )


def create_group(name):
    settings = {'name': name}
    return Group.objects.create(
            name=name,
            settings=settings,
            )


def create_item(name, user, is_public):
    metadata = {'title': name, 'author': user.username}
    settings = {'public': is_public}
    return Item.objects.create(
            owner=user,
            metadata=metadata,
            settings=settings
            )


def create_set(name, user, template, is_public):
    settings = {'name': name, 'public': is_public}
    return Set.objects.create(
            owner=user,
            template=template,
            settings=settings
            )
