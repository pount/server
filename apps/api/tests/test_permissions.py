# -*- coding: utf-8 -*-
from django.test import TestCase
from .util import create_user, create_group, create_item
from ..models import Role, Permission


class PermissionsTestCase(TestCase):

    def setUp(self):
        from ..models.init import initialize_roles
        initialize_roles()
        self.creator = create_user('creator', True)
        self.friends = create_user('friend', True)
        self.visitor = create_user('visitor', False)
        self.team = create_group('team')
        self.team.user_set.add(self.creator)
        self.team.user_set.add(self.friends)
        self.item_public = create_item('+i', self.creator, True)
        self.item_private = create_item('-i', self.creator, False)

    def tearDown(self):
        self.item_public.delete()
        self.item_private.delete()
        self.creator.delete()
        self.friends.delete()
        self.visitor.delete()


class RolesSanityTest(PermissionsTestCase):

    def test_roles(self):
        roles = Role.objects.all()
        self.assertEqual(3, roles.count())
        self.assertEqual('owner', str(roles[0]))
        self.assertEqual('contributor', str(roles[1]))
        self.assertEqual('member', str(roles[2]))

    def test_sanity(self):
        self.assertEqual('creator', self.item_public.owner.username)
        self.assertEqual('creator', self.item_private.owner.username)
        self.assertEqual('+i', self.item_public.title)
        self.assertEqual('-i', self.item_private.title)


class PermissionsFactoryTest(PermissionsTestCase):

    def test_unknown_role(self):
        self.assertEqual(3, Role.objects.all().count())
        with self.assertRaises(ValueError):
            Permission.create(user=None, set=None, role_name='unknown')
        self.assertEqual(3, Role.objects.all().count())

    def test_too_many_roles(self):
        self.assertEqual(3, Role.objects.all().count())
        Role.objects.create(name='bad actor', access_rights={})
        Role.objects.create(name='bad actor', access_rights={})
        self.assertEqual(5, Role.objects.all().count())
        with self.assertRaises(ValueError):
            Permission.create(user=None, set=None, role_name='bad actor')
        self.assertEqual(5, Role.objects.all().count())
