# -*- coding: utf-8 -*-
from django.test import TestCase
from ..models import Template, init


class TemplateTestCase(TestCase):

    def setUp(self):
        init.initialize_templates()

    def tearDown(self):
        Template.objects.all().delete()


class TemplatesTest(TemplateTestCase):

    def test_default_templates_count(self):
        templates = Template.objects.all()
        self.assertEqual(2, templates.count())

    def test_minimal(self):
        template = Template.objects.all()[0]
        self.assertEqual('minimal', template.name)
        self.assertEqual('minimal', str(template))
        self.assertEqual(1, len(template.metadata))

    def test_datacite(self):
        template = Template.objects.all()[1]
        self.assertEqual('datacite', template.name)
        self.assertEqual('datacite', str(template))
        self.assertEqual(16, len(template.metadata))
