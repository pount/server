# -*- coding: utf-8 -*-
from .test_permissions import PermissionsTestCase
from .util import create_user, create_item, create_set
from ..models import Template
from ..rules import (
        SET_VIEW, SET_EDIT, SET_CREATE, SET_DELETE,
        ITEM_VIEW, ITEM_EDIT, ITEM_DELETE,
        )


class SetPermissionsTestCase(PermissionsTestCase):

    def setUp(self):
        super(SetPermissionsTestCase, self).setUp()
        self.members = create_user('member', False)
        self.template = Template.objects.create(metadata={}, settings={})
        self.set_public = create_set('+s', self.creator, self.template, True)
        self.set_public.set_role(self.friends, 'contributor')
        self.set_public.set_role(self.members, 'member')
        self.set_private = create_set('-s', self.creator, self.template, False)
        self.set_private.set_role(self.friends, 'contributor')
        self.set_private.set_role(self.members, 'member')
        self.lone_public = create_item('+f', self.friends, True)
        self.lone_private = create_item('-f', self.friends, False)

    def tearDown(self):
        self.members.delete()
        self.set_public.delete()
        self.set_private.delete()
        self.template.delete()
        self.lone_public.delete()
        self.lone_private.delete()
        super(SetPermissionsTestCase, self).tearDown()


class SetPermissionsSanityTest(SetPermissionsTestCase):

    def test_sanity(self):
        self.assertEqual(0, self.set_public.items.count())
        self.assertEqual(0, self.set_private.items.count())
        self.assertEqual('creator', self.set_public.owner.username)
        self.assertEqual('creator', self.set_private.owner.username)
        self.assertEqual('creator', self.item_public.owner.username)
        self.assertEqual('creator', self.item_private.owner.username)
        self.assertEqual('friend', self.lone_public.owner.username)
        self.assertEqual('friend', self.lone_private.owner.username)

    def test_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_CREATE))
        self.assertTrue(self.friends.has_perm(SET_CREATE))
        self.assertFalse(self.members.has_perm(SET_CREATE))
        self.assertFalse(self.visitor.has_perm(SET_CREATE))

    def test_roles(self):
        self.assertEqual('owner', self.set_public.get_role(self.creator))
        self.assertEqual('contributor', self.set_public.get_role(self.friends))
        self.assertEqual('member', self.set_public.get_role(self.members))
        self.assertIsNone(self.set_public.get_role(self.visitor))

#
#
#
#
#
###############################################################################
#
#
#
#
#


class PublicItemAddedToSameOwnerPublicSetTest(SetPermissionsTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.set_public.items.add(self.item_public)

    def test_sanity(self):
        self.assertEqual(1, self.set_public.items.count())
        self.assertEqual('creator', self.set_public.owner.username)
        self.assertEqual('creator', self.item_public.owner.username)

    def test_creator_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_VIEW, self.set_public))
        self.assertTrue(self.creator.has_perm(SET_EDIT, self.set_public))
        self.assertTrue(self.creator.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.creator.has_perm(ITEM_VIEW, self.item_public))
        self.assertTrue(self.creator.has_perm(ITEM_EDIT, self.item_public))
        self.assertTrue(self.creator.has_perm(ITEM_DELETE, self.item_public))

    def test_contributor_permissions(self):
        self.assertTrue(self.friends.has_perm(SET_VIEW, self.set_public))
        self.assertTrue(self.friends.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.friends.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.friends.has_perm(ITEM_VIEW, self.item_public))
        self.assertTrue(self.friends.has_perm(ITEM_EDIT, self.item_public))
        self.assertFalse(self.friends.has_perm(ITEM_DELETE, self.item_public))

    def test_member_permissions(self):
        self.assertTrue(self.members.has_perm(SET_VIEW, self.set_public))
        self.assertFalse(self.members.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.members.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.members.has_perm(ITEM_VIEW, self.item_public))
        self.assertFalse(self.members.has_perm(ITEM_EDIT, self.item_public))
        self.assertFalse(self.members.has_perm(ITEM_DELETE, self.item_public))

    def test_visitor_permissions(self):
        self.assertTrue(self.visitor.has_perm(SET_VIEW, self.set_public))
        self.assertFalse(self.visitor.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.visitor.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.visitor.has_perm(ITEM_VIEW, self.item_public))
        self.assertFalse(self.visitor.has_perm(ITEM_EDIT, self.item_public))
        self.assertFalse(self.visitor.has_perm(ITEM_DELETE, self.item_public))


class PublicItemAddedToSameOwnerPrivateSetTest(SetPermissionsTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.set_private.items.add(self.item_public)

    def test_sanity(self):
        self.assertEqual(1, self.set_private.items.count())
        self.assertEqual('creator', self.set_private.owner.username)
        self.assertEqual('creator', self.item_public.owner.username)

    def test_creator_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_VIEW, self.set_private))
        self.assertTrue(self.creator.has_perm(SET_EDIT, self.set_private))
        self.assertTrue(self.creator.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.creator.has_perm(ITEM_VIEW, self.item_public))
        self.assertTrue(self.creator.has_perm(ITEM_EDIT, self.item_public))
        self.assertTrue(self.creator.has_perm(ITEM_DELETE, self.item_public))

    def test_contributor_permissions(self):
        self.assertTrue(self.friends.has_perm(SET_VIEW, self.set_private))
        self.assertTrue(self.friends.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.friends.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.friends.has_perm(ITEM_VIEW, self.item_public))
        self.assertTrue(self.friends.has_perm(ITEM_EDIT, self.item_public))
        self.assertFalse(self.friends.has_perm(ITEM_DELETE, self.item_public))

    def test_member_permissions(self):
        self.assertTrue(self.members.has_perm(SET_VIEW, self.set_private))
        self.assertFalse(self.members.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.members.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.members.has_perm(ITEM_VIEW, self.item_public))
        self.assertFalse(self.members.has_perm(ITEM_EDIT, self.item_public))
        self.assertFalse(self.members.has_perm(ITEM_DELETE, self.item_public))

    def test_visitor_permissions(self):
        self.assertFalse(self.visitor.has_perm(SET_VIEW, self.set_private))
        self.assertFalse(self.visitor.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.visitor.has_perm(SET_DELETE, self.set_private))
        self.assertFalse(self.visitor.has_perm(ITEM_VIEW, self.item_public))
        self.assertFalse(self.visitor.has_perm(ITEM_EDIT, self.item_public))
        self.assertFalse(self.visitor.has_perm(ITEM_DELETE, self.item_public))


class PrivateItemAddedToSameOwnerPrivateSetTest(SetPermissionsTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.set_private.items.add(self.item_private)

    def test_sanity(self):
        self.assertEqual(1, self.set_private.items.count())
        self.assertEqual('creator', self.set_private.owner.username)
        self.assertEqual('creator', self.item_private.owner.username)

    def test_creator_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_VIEW, self.set_private))
        self.assertTrue(self.creator.has_perm(SET_EDIT, self.set_private))
        self.assertTrue(self.creator.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.creator.has_perm(ITEM_VIEW, self.item_private))
        self.assertTrue(self.creator.has_perm(ITEM_EDIT, self.item_private))
        self.assertTrue(self.creator.has_perm(ITEM_DELETE, self.item_private))

    def test_contributor_permissions(self):
        self.assertTrue(self.friends.has_perm(SET_VIEW, self.set_private))
        self.assertTrue(self.friends.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.friends.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.friends.has_perm(ITEM_VIEW, self.item_private))
        self.assertTrue(self.friends.has_perm(ITEM_EDIT, self.item_private))
        self.assertFalse(self.friends.has_perm(ITEM_DELETE, self.item_private))

    def test_member_permissions(self):
        self.assertTrue(self.members.has_perm(SET_VIEW, self.set_private))
        self.assertFalse(self.members.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.members.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.members.has_perm(ITEM_VIEW, self.item_private))
        self.assertFalse(self.members.has_perm(ITEM_EDIT, self.item_private))
        self.assertFalse(self.members.has_perm(ITEM_DELETE, self.item_private))

    def test_visitor_permissions(self):
        self.assertFalse(self.visitor.has_perm(SET_VIEW, self.set_private))
        self.assertFalse(self.visitor.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.visitor.has_perm(SET_DELETE, self.set_private))
        self.assertFalse(self.visitor.has_perm(ITEM_VIEW, self.item_private))
        self.assertFalse(self.visitor.has_perm(ITEM_EDIT, self.item_private))
        self.assertFalse(self.visitor.has_perm(ITEM_DELETE, self.item_private))


class PrivateItemAddedToSameOwnerPublicSetTest(SetPermissionsTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.set_public.items.add(self.item_private)

    def test_sanity(self):
        self.assertEqual(1, self.set_public.items.count())
        self.assertEqual('creator', self.set_public.owner.username)
        self.assertEqual('creator', self.item_private.owner.username)

    def test_creator_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_VIEW, self.set_public))
        self.assertTrue(self.creator.has_perm(SET_EDIT, self.set_public))
        self.assertTrue(self.creator.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.creator.has_perm(ITEM_VIEW, self.item_private))
        self.assertTrue(self.creator.has_perm(ITEM_EDIT, self.item_private))
        self.assertTrue(self.creator.has_perm(ITEM_DELETE, self.item_private))

    def test_contributor_permissions(self):
        self.assertTrue(self.friends.has_perm(SET_VIEW, self.set_public))
        self.assertTrue(self.friends.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.friends.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.friends.has_perm(ITEM_VIEW, self.item_private))
        self.assertTrue(self.friends.has_perm(ITEM_EDIT, self.item_private))
        self.assertFalse(self.friends.has_perm(ITEM_DELETE, self.item_private))

    def test_member_permissions(self):
        self.assertTrue(self.members.has_perm(SET_VIEW, self.set_public))
        self.assertFalse(self.members.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.members.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.members.has_perm(ITEM_VIEW, self.item_private))
        self.assertFalse(self.members.has_perm(ITEM_EDIT, self.item_private))
        self.assertFalse(self.members.has_perm(ITEM_DELETE, self.item_private))

    def test_visitor_permissions(self):
        self.assertTrue(self.visitor.has_perm(SET_VIEW, self.set_public))
        self.assertFalse(self.visitor.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.visitor.has_perm(SET_DELETE, self.set_public))
        self.assertFalse(self.visitor.has_perm(ITEM_VIEW, self.item_private))
        self.assertFalse(self.visitor.has_perm(ITEM_EDIT, self.item_private))
        self.assertFalse(self.visitor.has_perm(ITEM_DELETE, self.item_private))

#
#
#
#
#
###############################################################################
#
#
#
#
#


class PublicItemAddedToOtherOwnerPublicSetTest(SetPermissionsTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.set_public.items.add(self.lone_public)

    def test_sanity(self):
        self.assertEqual(1, self.set_public.items.count())
        self.assertEqual('creator', self.set_public.owner.username)
        self.assertEqual('friend', self.lone_public.owner.username)

    def test_creator_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_VIEW, self.set_public))
        self.assertTrue(self.creator.has_perm(SET_EDIT, self.set_public))
        self.assertTrue(self.creator.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.creator.has_perm(ITEM_VIEW, self.lone_public))
        self.assertTrue(self.creator.has_perm(ITEM_EDIT, self.lone_public))
        self.assertTrue(self.creator.has_perm(ITEM_DELETE, self.lone_public))

    def test_contributor_permissions(self):
        self.assertTrue(self.friends.has_perm(SET_VIEW, self.set_public))
        self.assertTrue(self.friends.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.friends.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.friends.has_perm(ITEM_VIEW, self.lone_public))
        self.assertTrue(self.friends.has_perm(ITEM_EDIT, self.lone_public))
        self.assertTrue(self.friends.has_perm(ITEM_DELETE, self.lone_public))

    def test_member_permissions(self):
        self.assertTrue(self.members.has_perm(SET_VIEW, self.set_public))
        self.assertFalse(self.members.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.members.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.members.has_perm(ITEM_VIEW, self.lone_public))
        self.assertFalse(self.members.has_perm(ITEM_EDIT, self.lone_public))
        self.assertFalse(self.members.has_perm(ITEM_DELETE, self.lone_public))

    def test_visitor_permissions(self):
        self.assertTrue(self.visitor.has_perm(SET_VIEW, self.set_public))
        self.assertFalse(self.visitor.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.visitor.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.visitor.has_perm(ITEM_VIEW, self.lone_public))
        self.assertFalse(self.visitor.has_perm(ITEM_EDIT, self.lone_public))
        self.assertFalse(self.visitor.has_perm(ITEM_DELETE, self.lone_public))


class PublicItemAddedToOtherOwnerPrivateSetTest(SetPermissionsTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.set_private.items.add(self.lone_public)

    def test_sanity(self):
        self.assertEqual(1, self.set_private.items.count())
        self.assertEqual('creator', self.set_private.owner.username)
        self.assertEqual('friend', self.lone_public.owner.username)

    def test_creator_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_VIEW, self.set_private))
        self.assertTrue(self.creator.has_perm(SET_EDIT, self.set_private))
        self.assertTrue(self.creator.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.creator.has_perm(ITEM_VIEW, self.lone_public))
        self.assertTrue(self.creator.has_perm(ITEM_EDIT, self.lone_public))
        self.assertTrue(self.creator.has_perm(ITEM_DELETE, self.lone_public))

    def test_contributor_permissions(self):
        self.assertTrue(self.friends.has_perm(SET_VIEW, self.set_private))
        self.assertTrue(self.friends.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.friends.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.friends.has_perm(ITEM_VIEW, self.lone_public))
        self.assertTrue(self.friends.has_perm(ITEM_EDIT, self.lone_public))
        self.assertTrue(self.friends.has_perm(ITEM_DELETE, self.lone_public))

    def test_member_permissions(self):
        self.assertTrue(self.members.has_perm(SET_VIEW, self.set_private))
        self.assertFalse(self.members.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.members.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.members.has_perm(ITEM_VIEW, self.lone_public))
        self.assertFalse(self.members.has_perm(ITEM_EDIT, self.lone_public))
        self.assertFalse(self.members.has_perm(ITEM_DELETE, self.lone_public))

    def test_visitor_permissions(self):
        self.assertFalse(self.visitor.has_perm(SET_VIEW, self.set_private))
        self.assertFalse(self.visitor.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.visitor.has_perm(SET_DELETE, self.set_private))
        self.assertFalse(self.visitor.has_perm(ITEM_VIEW, self.lone_public))
        self.assertFalse(self.visitor.has_perm(ITEM_EDIT, self.lone_public))
        self.assertFalse(self.visitor.has_perm(ITEM_DELETE, self.lone_public))


class PrivateItemAddedToOtherOwnerPrivateSetTest(SetPermissionsTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.set_private.items.add(self.lone_private)

    def test_sanity(self):
        self.assertEqual(1, self.set_private.items.count())
        self.assertEqual('creator', self.set_private.owner.username)
        self.assertEqual('friend', self.lone_private.owner.username)

    def test_creator_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_VIEW, self.set_private))
        self.assertTrue(self.creator.has_perm(SET_EDIT, self.set_private))
        self.assertTrue(self.creator.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.creator.has_perm(ITEM_VIEW, self.lone_private))
        self.assertTrue(self.creator.has_perm(ITEM_EDIT, self.lone_private))
        self.assertTrue(self.creator.has_perm(ITEM_DELETE, self.lone_private))

    def test_contributor_permissions(self):
        self.assertTrue(self.friends.has_perm(SET_VIEW, self.set_private))
        self.assertTrue(self.friends.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.friends.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.friends.has_perm(ITEM_VIEW, self.lone_private))
        self.assertTrue(self.friends.has_perm(ITEM_EDIT, self.lone_private))
        self.assertTrue(self.friends.has_perm(ITEM_DELETE, self.lone_private))

    def test_member_permissions(self):
        self.assertTrue(self.members.has_perm(SET_VIEW, self.set_private))
        self.assertFalse(self.members.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.members.has_perm(SET_DELETE, self.set_private))
        self.assertTrue(self.members.has_perm(ITEM_VIEW, self.lone_private))
        self.assertFalse(self.members.has_perm(ITEM_EDIT, self.lone_private))
        self.assertFalse(self.members.has_perm(ITEM_DELETE, self.lone_private))

    def test_visitor_permissions(self):
        self.assertFalse(self.visitor.has_perm(SET_VIEW, self.set_private))
        self.assertFalse(self.visitor.has_perm(SET_EDIT, self.set_private))
        self.assertFalse(self.visitor.has_perm(SET_DELETE, self.set_private))
        self.assertFalse(self.visitor.has_perm(ITEM_VIEW, self.lone_private))
        self.assertFalse(self.visitor.has_perm(ITEM_EDIT, self.lone_private))
        self.assertFalse(self.visitor.has_perm(ITEM_DELETE, self.lone_private))


class PrivateItemAddedToOtherOwnerPublicSetTest(SetPermissionsTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.set_public.items.add(self.lone_private)

    def test_sanity(self):
        self.assertEqual(1, self.set_public.items.count())
        self.assertEqual('creator', self.set_public.owner.username)
        self.assertEqual('friend', self.lone_private.owner.username)

    def test_creator_permissions(self):
        self.assertTrue(self.creator.has_perm(SET_VIEW, self.set_public))
        self.assertTrue(self.creator.has_perm(SET_EDIT, self.set_public))
        self.assertTrue(self.creator.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.creator.has_perm(ITEM_VIEW, self.lone_private))
        self.assertTrue(self.creator.has_perm(ITEM_EDIT, self.lone_private))
        self.assertTrue(self.creator.has_perm(ITEM_DELETE, self.lone_private))

    def test_contributor_permissions(self):
        self.assertTrue(self.friends.has_perm(SET_VIEW, self.set_public))
        self.assertTrue(self.friends.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.friends.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.friends.has_perm(ITEM_VIEW, self.lone_private))
        self.assertTrue(self.friends.has_perm(ITEM_EDIT, self.lone_private))
        self.assertTrue(self.friends.has_perm(ITEM_DELETE, self.lone_private))

    def test_member_permissions(self):
        self.assertTrue(self.members.has_perm(SET_VIEW, self.set_public))
        self.assertFalse(self.members.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.members.has_perm(SET_DELETE, self.set_public))
        self.assertTrue(self.members.has_perm(ITEM_VIEW, self.lone_private))
        self.assertFalse(self.members.has_perm(ITEM_EDIT, self.lone_private))
        self.assertFalse(self.members.has_perm(ITEM_DELETE, self.lone_private))

    def test_visitor_permissions(self):
        self.assertTrue(self.visitor.has_perm(SET_VIEW, self.set_public))
        self.assertFalse(self.visitor.has_perm(SET_EDIT, self.set_public))
        self.assertFalse(self.visitor.has_perm(SET_DELETE, self.set_public))
        self.assertFalse(self.visitor.has_perm(ITEM_VIEW, self.lone_private))
        self.assertFalse(self.visitor.has_perm(ITEM_EDIT, self.lone_private))
        self.assertFalse(self.visitor.has_perm(ITEM_DELETE, self.lone_private))
