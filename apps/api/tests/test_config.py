# -*- coding: utf-8 -*-
from django.apps import apps
from django.test import TestCase
from ..apps import AppConfig


class ItemsAppConfigTest(TestCase):

    def test_apps(self):
        self.assertEqual('api', AppConfig.name)
        qualified_name = apps.get_app_config('api').name
        self.assertEqual('apps.api', qualified_name)
