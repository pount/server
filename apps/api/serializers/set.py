# -*- coding: utf-8 -*-
from ..models import Set
from rest_framework import serializers


class SetSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Set
        fields = [
                'url',
                'template',
                'settings',
                'items',
                'permissions',
                'owner',
                ]
