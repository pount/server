# -*- coding: utf-8 -*-
from ..models import Template
from rest_framework import serializers


class TemplateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Template
        fields = ['url', 'metadata', 'settings', ]
