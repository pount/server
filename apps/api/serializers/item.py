# -*- coding: utf-8 -*-
from ..models import Item
from rest_framework import serializers
from django.conf import settings


class ItemSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Item
        fields = ['url', 'metadata', 'settings', 'set', 'owner', ]

    def validate_settings(self, settings: dict) -> dict:
        errors = []
        self.validate_settings_source(settings.get('source', None), errors)
        if errors:
            raise serializers.ValidationError(errors)
        return settings

    def validate_settings_source(self, source: dict, errors: list) -> None:
        """
        Custom validation method called by validate_settings.
        """
        if source is not None:
            url = source.get('url', None)
            if not url:
                errors.append("'source.url' required.")
            else:
                validate_url(url, errors)
            service = source.get('service', None)
            importer = get_importer(service, errors)
            if not errors:
                if importer:
                    self.zenodo_response = importer.fetch_validate(url, errors)
        else:
            errors.append("'source' required.")


def validate_url(url: str, errors: list) -> None:
    from django.core.validators import URLValidator
    from django.core.exceptions import ValidationError
    validate = URLValidator()
    try:
        validate(url)
    except ValidationError:
        errors.append("'source.url': %s must be a valid URL." % url)


def get_importer(name: str, errors: list):
    classname = settings.ITEM_IMPORTERS.get(name, None)
    if classname:
        from importlib import import_module
        # This can raise ModuleNotFoundError. Then, server will error 500.
        # We could try/except it and return None, but it would create the
        # same ValidationError (ie. error 400) as if importer name was wrong.
        # As ModuleNotFoundError only occurs if ITEM_IMPORTERS is badly set,
        # this would really be a configuration problem.
        # Thus, let's be honest with caller and error 500 in this case.
        return import_module(classname)
    importers = settings.ITEM_IMPORTERS.keys()
    errors.append("'source.service' must be one of: %s." % importers)
    return None
