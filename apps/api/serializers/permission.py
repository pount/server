# -*- coding: utf-8 -*-
from ..models import Role, Permission
from rest_framework import serializers


class RoleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Role
        fields = ['url', 'name', 'access_rights', ]


class PermissionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Permission
        fields = ['url', 'user', 'role', 'set', ]
