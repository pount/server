# -*- coding: utf-8 -*-
from django.db.models import Model, CharField, ForeignKey, CASCADE, PROTECT
from django.contrib.postgres.fields import JSONField
from .set import Set
from django.contrib.auth import get_user_model
User = get_user_model()


class Role(Model):
    name = CharField(max_length=64)
    access_rights = JSONField()
    # +permissions, see .permission.Permission

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.name


class Permission(Model):
    user = ForeignKey(User, related_name='permissions',
                      null=False, on_delete=CASCADE)
    role = ForeignKey(Role, related_name='permissions',
                      null=False, on_delete=PROTECT)
    set = ForeignKey(Set, related_name='permissions',
                     null=False, on_delete=CASCADE)

    @staticmethod
    def create(user, set, role_name):
        role = Role.objects.filter(name=role_name)
        if not role.exists():
            raise ValueError("Unknown role '%s'" % role_name)
        if role.count() > 1:
            raise ValueError("Too many roles for '%s'" % role_name)
        role = role.first()
        return Permission(user=user, set=set, role=role)
