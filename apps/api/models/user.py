# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import JSONField


class User(AbstractUser):
    '''
    Yeah, there's a built-in User model for authentication in Django.
    However it is "highly recommended" to use a custom user model, because
    updating this custom model is quite easy compared to updating the default
    User model which is quite a PITA.
    @see https://docs.djangoproject.com/en/3.0/topics/auth/customizing/
         #using-a-custom-user-model-when-starting-a-project
    '''
    info = JSONField(default=dict)
    settings = JSONField(default=dict)
    # +permissions, see .permission.Permission

    @property
    def is_creator(self):
        return self.settings.get('creator', False)
