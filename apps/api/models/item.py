# -*- coding: utf-8 -*-
from django.db.models import Model, ForeignKey, CASCADE
from django.contrib.postgres.fields import JSONField
from django.contrib.auth import get_user_model
from .set import Set
User = get_user_model()


class Item(Model):
    metadata = JSONField()
    settings = JSONField()
    set = ForeignKey(Set, related_name='items',
                     null=True, on_delete=CASCADE)
    owner = ForeignKey(User, related_name='items',
                       null=False, on_delete=CASCADE)

    class Meta:
        ordering = ['id']

    @property
    def title(self):
        return self.metadata.get('title', None)

    @property
    def is_public(self):
        return self.settings.get('public', False)
