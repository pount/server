# -*- coding: utf-8 -*-
from django.db.models import Model, ForeignKey, PROTECT, Q
from django.contrib.postgres.fields import JSONField
from django.db.models.signals import post_save
from .template import Template
from django.contrib.auth import get_user_model
User = get_user_model()


class Set(Model):
    template = ForeignKey(Template, related_name='sets',
                          null=False, on_delete=PROTECT)
    settings = JSONField()
    # +items, see .item.Item
    # +permissions, see .permission.Permission
    owner = ForeignKey(User, related_name='sets',
                       null=False, on_delete=PROTECT)

    class Meta:
        ordering = ['id']

    @property
    def is_public(self):
        return self.settings.get('public', False)

    def get_role(self, user):
        query = self.permissions.filter(user=user)
        if query.count() > 1:
            raise ValueError('User "%s": too many roles !')
        if query.exists():
            return query.first().role.name
        return None

    def set_role(self, user, role_name):
        permissions = self.permissions.filter(user=user)
        now = permissions.filter(role__name=role_name)
        old = permissions.filter(~Q(role__name=role_name))
        dirty = False
        for permission in old.all():  # remove all old permissions, if any
            dirty = True
            permission.delete()
        if not now.exists():  # create new permissions
            from .permission import Permission
            dirty = True
            permission = Permission.create(user, self, role_name)
            permission.save()
            self.permissions.add(permission)
        if dirty:  # save only if necessary, or else we risk infinite loop
            self.save()

    @staticmethod
    def update_owner_permissions(sender, **kwargs):
        instance = kwargs.get('instance')
        instance.set_role(instance.owner, 'owner')


# See: https://docs.djangoproject.com/fr/3.0/ref/signals/
post_save.connect(Set.update_owner_permissions, sender=Set)
