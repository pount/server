# -*- coding: utf-8 -*-
from .template import Template
from .permission import Role
from ..rules import (
            SET_DELETE,
            SET_VIEW,
            SET_EDIT,
            ITEM_CREATE,
            ITEM_DELETE,
            ITEM_VIEW,
            ITEM_EDIT,
        )


def initialize_roles():
    rights = {
            SET_DELETE: True,
            SET_VIEW: True,
            SET_EDIT: True,
            ITEM_CREATE: True,
            ITEM_DELETE: True,
            ITEM_VIEW: True,
            ITEM_EDIT: True,
            }
    if not Role.objects.filter(name='owner').exists():
        Role.objects.create(name='owner', access_rights=rights)

    rights[SET_DELETE] = False
    rights[SET_EDIT] = False
    rights[ITEM_DELETE] = False
    if not Role.objects.filter(name='contributor').exists():
        Role.objects.create(name='contributor', access_rights=rights)

    rights[ITEM_CREATE] = False
    rights[ITEM_EDIT] = False
    if not Role.objects.filter(name='member').exists():
        Role.objects.create(name='member', access_rights=rights)


def initialize_templates():
    if not Template.objects.filter(settings__name='minimal').exists():
        metadata = {
                'title': 'string',
                }
        settings = {'name': 'minimal'}
        Template.objects.create(metadata=metadata, settings=settings)
    if not Template.objects.filter(settings__name='datacite').exists():
        metadata = {
                'identifier': None,
                'creator': None,
                'title': 'string',
                'publisher': None,
                'publicationYear': None,
                'subject': None,
                'contributor': None,
                'date': None,
                'language': None,
                'resourceType': None,
                'alternateIdentifier': None,
                'relatedIdentifier': None,
                'version': None,
                'description': None,
                'geolocation': None,
                'fundingReference': None,
                }
        settings = {'name': 'datacite'}
        Template.objects.create(metadata=metadata, settings=settings)
