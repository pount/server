# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group as DjangoGroup
from django.contrib.postgres.fields import JSONField


class Group(DjangoGroup):
    settings = JSONField()
