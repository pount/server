# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.postgres.fields import JSONField


class Template(models.Model):
    metadata = JSONField()
    settings = JSONField()

    class Meta:
        ordering = ['id']

    @property
    def name(self):
        return self.settings.get('name', None)

    def __str__(self):
        return self.name
