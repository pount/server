# -*- coding: utf-8 -*-
from django.urls import include, path
from rest_framework import routers
from . import views
from . import rules

router = routers.DefaultRouter()
router.register(r'items', views.ItemViewSet)
router.register(r'sets', views.SetViewSet)
router.register(r'templates', views.TemplateViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'roles', views.RoleViewSet)
router.register(r'permissions', views.PermissionViewSet)

urlpatterns = [
  path('', include(router.urls)),
]

# Django imports and executes each module top-level urls.py only once.
# Thus, this is where we put one-time startup stuff like this.
rules.initialize_rules()
