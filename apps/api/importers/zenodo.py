# -*- coding: utf-8 -*-
from rest_framework.views import status
import requests


def fetch(url):
    return requests.get(url)


def fetch_validate(url, errors):
    response = fetch(url)
    code = response.status_code
    if code != status.HTTP_200_OK:
        errors.append('Received Error %s from %s' % (code, url))
    return response


def translate(response):
    if response.status_code != status.HTTP_200_OK:
        return response
    # json_data = response.json()
    # TODO only keep metadata and files
    # self.data['metadata'] = json_data['metadata']
    # self.data['files'] = json_data['files']
    return response
