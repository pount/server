# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from elasticsearch_dsl import connections
from django.conf import settings
import logging
logger = logging.getLogger(__name__)


def create_index(index):
    connection = Elasticsearch(settings.ELASTICSEARCH_URL)
    logger.debug('create_index(%s) ...' % index)
    try:
        if connection.indices.exists(index):
            logger.info("Deleting '%s' index" % index)
            response = connection.indices.delete(index=index)
            logger.info("Response: '%s'." % response)
    except Exception as ex:
        logger.exception('ERROR during create_index(%s): %s', index, ex)


def bulk_indexing(index):
    connections.create_connection(settings.ELASTICSEARCH_URL)
    connection = Elasticsearch(settings.ELASTICSEARCH_URL)
    try:
        from .documents import Item as ItemIndex
        # exists = connection.indices.exists(index=index)
        ItemIndex.init(index=index, using=connection)
    except ValueError as ex:
        logger.exception('ERROR during bulk_indexing: %s', ex)

    from apps.api import models
    actions = (
        ItemIndex.create(item, using=connection, index=index)
        for item in models.Item.objects.all())
    bulk(client=connection, actions=actions, refresh=True)
