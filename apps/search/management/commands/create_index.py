# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from apps.search import create_index, bulk_indexing


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        create_index('pount')
        bulk_indexing('pount')
