# -*- coding: utf-8 -*-
from elasticsearch_dsl import Document
from apps.api.models import Item as ItemModel


class Item(Document):
    """Item, as indexed by Elasticsearch"""
    class Meta:
        model = ItemModel

    @staticmethod
    def create(model, using=None, index=None):
        """Factory method.
        Creates an Elasticsearch document from a Django Model.
        In other words, it converts an instance of
        :class:`apps.api.models.Item`
        into an instance of :class:`apps.search.documents.Item`.

        :param model: :class:`apps.api.models.Item` object to convert from.
        :param using: :class:`elasticsearch.Elasticsearch` connection to use.
        :param index: Name of the index corresponding to the
                      :class:`apps.search.documents.Item` object to be created.

        :return: New Elasticsearch document corresponding to ``model``.
        :rtype: :class:`apps.search.documents.Item`
        """
        document = Item()
        document.id = model.id
        document.name = model.name
        document.owner = model.owner
        document.save(using=using, index=index)
        return document.to_dict(include_meta=True)
