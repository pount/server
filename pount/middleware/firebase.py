import logging
from os.path import join
from django.conf import settings
from django.contrib.auth import models, get_user_model
from rest_framework.exceptions import AuthenticationFailed
from firebase_admin import initialize_app, auth, credentials
User = get_user_model()
logger = logging.getLogger(__name__)


CERTIFICATE_PATH = join(settings.SITE_ROOT, 'keys', 'pount-dev-firebase.json')
try:
    initialize_app(credentials.Certificate(CERTIFICATE_PATH))
except (FileNotFoundError, ValueError):
    message = 'Firebase initialization failure: social sign-in is broken'
    logger.warn(message, exc_info=True)


class FirebaseAuthenticationMiddleware(object):

    KEYWORD = 'FirebaseToken'

    def __init__(self, get_response=None):
        # old-style middlewares accept no argument, thus allow None
        self.get_response = get_response

    def __call__(self, request):
        # this method is only for new-style middlewares
        response = self.process_request(request)
        if response is None:
            # we must call the next middleware (or the view)
            response = self.get_response(request)
        response = self.process_response(request, response)
        return response

    def process_response(self, request, response):
        # handle old-style response processing
        # Do something with response, possibly using request.
        return response

    def process_request(self, request):
        # django.contrib.sessions.middleware.SessionMiddleware' required.
        # If you get this error, check your MIDDLEWARE_CLASSES in settings.
        assert hasattr(request, 'session')
        token = self.get_token(request)
        if token:
            credentials = self.authenticate(token)
            request.user = self.get_or_create(credentials)
        else:
            request.user = models.AnonymousUser()

    def get_token(self, request):
        header = request.headers.get('Authorization', None)
        if not header:
            return None
        parts = header.split()
        if not parts or parts[0].lower() != self.KEYWORD.lower():
            return None  # probably another authorization protocol ...
        if len(parts) == 1 or len(parts) > 2:
            error = 'Invalid Google access token in Authorization header'
            raise AuthenticationFailed(error)
        return parts[1].strip()

    def authenticate(self, token):
        try:
            credentials = auth.verify_id_token(token)
        except (
                ValueError,
                auth.InvalidIdTokenError,
                auth.ExpiredIdTokenError,
                auth.RevokedIdTokenError,
                ):
            raise AuthenticationFailed('Invalid token')
        # return this as an alternative to plain old dict 'credentials':
        # record = auth.get_user(credentials['uid'])
        return credentials

    def get_or_create(self, credentials):
        uid = credentials['uid']
        already_here = User.objects.filter(info__uid=uid)
        if len(already_here) > 0:
            # there should never be more than one user with this uid,
            # because a few line below is the ONLY place we create a new user,
            # and we take care never to add a second user with same uid.
            return already_here[0]
        # create User in database
        # /!\ for leagal reasons, your client should inform end-users
        #     that Pount creates an account in its API
        keys_we_want = ['uid', 'name', 'picture', 'email', ]
        info = {key: credentials[key] for key in keys_we_want}
        settings = {'creator': True, }  # TODO: social accounts --> False
        username = info['name']
        user = User.objects.create_user(
                username=username,
                info=info,
                settings=settings
                )
        user.save()
        logger.info('Created user "%s"[%s]' % (username, uid))
        return user
