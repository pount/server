# -*- coding: utf-8 -*-
from .firebase import FirebaseAuthenticationMiddleware

__all__ = [
  'FirebaseAuthenticationMiddleware',
]
