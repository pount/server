# -*- coding: utf-8 -*-
from unittest.mock import patch
from django.test import TestCase, RequestFactory
from django.contrib.auth import get_user_model
from rest_framework.exceptions import AuthenticationFailed
from ..firebase import FirebaseAuthenticationMiddleware


FIREBASE_VERIFY = 'pount.middleware.firebase.auth.verify_id_token'

user_info_stub = {
        'uid': 'whatever',
        'name': 'Jane Doe',
        'picture': 'image.jpg',
        'email': 'jane.doe@pount.org',
        }


def mock_verify(token):
    return user_info_stub


class FirebaseMiddlewareTest(TestCase):

    def setUp(self):
        def stub_get_response(request):
            return None
        self.request = RequestFactory().get('/whatever/')
        self.request.session = 'whatever'
        self.middleware = FirebaseAuthenticationMiddleware(stub_get_response)

    def test_init(self):
        middleware = FirebaseAuthenticationMiddleware('whatever')
        self.assertEqual('whatever', middleware.get_response)

    def test_no_authorization_header(self):
        self.middleware(self.request)
        self.assertTrue(self.request.user.is_anonymous)
        self.assertFalse(self.request.user.is_authenticated)
        self.assertEqual(0, get_user_model().objects.count())

    def test_empty_authorization_header(self):
        self.request.META['HTTP_AUTHORIZATION'] = ''
        self.middleware(self.request)
        self.assertTrue(self.request.user.is_anonymous)
        self.assertFalse(self.request.user.is_authenticated)
        self.assertEqual(0, get_user_model().objects.count())

    def test_wrong_keyword_in_authorization_header(self):
        self.request.META['HTTP_AUTHORIZATION'] = 'WrongKeyword token'
        self.middleware(self.request)
        self.assertTrue(self.request.user.is_anonymous)
        self.assertFalse(self.request.user.is_authenticated)
        self.assertEqual(0, get_user_model().objects.count())

    def test_no_token_in_authorization_header(self):
        self.request.META['HTTP_AUTHORIZATION'] = 'FirebaseToken '
        with self.assertRaises(AuthenticationFailed):
            self.middleware(self.request)
        self.assertEqual(0, get_user_model().objects.count())

    def test_invalid_token(self):
        token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc2MjNlMTBhMDQ1MTQwZjFjZmQ0YmUwNDY2Y2Y4MDM1MmI1OWY4MWUiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiTsOpZ1lwJHdneHoiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDUvZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1WLXB0V0Mxc3p3ay9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BTVp1dWNsajVvTUowWHBpZ2JUbEdwbWJNMW9QSWNZaXZRL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9wb3VudC0yNTg0OTc2NzU2NzEzIiwiYXVkIjoicG91bnQtMTU4NDk3Njc1NjcxMyIsImF1dGhfdGltZSI6MTU5NDIwNDY5NiwidXNlcl9pZCI6IlhFQ2s0OllheWRodl1YWmhaVEQ3NVBEW3hTeDIiLCJzdWIiOiJZQ1NLNFZZZW1kZmJNWDp4WlQ0OEVRRGd4VHgxIiwiaWF0IjoxNTk0MjA0Njk2LCJleHAiOjE1OTQyMDgyOTYsImVtYWlsIjoidnVtbXNta3l1ekBncVFqcC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjFuNzB2NkYxXlJ2NmxxNTM3PTAxMCJdLCJlbWFpbCI6WyJzZWctdy54bXR6QGdtYWltLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.km5NIqBgz-CuSw9O5pVmGUJNd0o5xWhufaXKcxQecLsKYzJe1wfSv4oV-u_4Mm_fW7QMsyK25QhQX6gCSQCGhwAgNFxMiNDCKbkeVyPCq9zCFfkk9xP4tMo3fmIfjMwI__E0S0QFi6eRS7xBAK86TLuB0a5wTrppgIMcEOisAPIgF0PNEvqSxY1qH2_TwZhAfIscTlFdqChdSHjjUcCPnZnlfkdfBzcJXtjB06_083CR1Rz9jcYjHqNSnAssL_Wx3xok95VDbIHOZAzIStpjtjg8umxBYNAlwzSAoZcGMoKRLmARAB43mwj22bb5aIp3qPWvUPAf5VqLS_m4dNWh_Q'  # noqa: E501
        self.request.META['HTTP_AUTHORIZATION'] = 'FirebaseToken %s' % token
        with self.assertRaises(AuthenticationFailed):
            self.middleware(self.request)
        self.assertEqual(0, get_user_model().objects.count())

    @patch(FIREBASE_VERIFY, side_effect=mock_verify)
    def test_first_time_user_created(self, verify_id_token):
        self.request.META['HTTP_AUTHORIZATION'] = 'FirebaseToken testtoken'
        self.assertEqual(0, get_user_model().objects.count())
        self.middleware(self.request)
        self.assertEqual(1, get_user_model().objects.count())
        self.assertFalse(self.request.user.is_anonymous)
        self.assertTrue(self.request.user.is_authenticated)
        user = get_user_model().objects.first()
        self.assertFalse(user.is_anonymous)
        self.assertTrue(user.is_authenticated)

    @patch(FIREBASE_VERIFY, side_effect=mock_verify)
    def test_existing_user_not_recreated(self, verify_id_token):
        get_user_model().objects.create_user(
                username=user_info_stub['name'],
                info=user_info_stub,
                settings={},
                )
        self.request.META['HTTP_AUTHORIZATION'] = 'FirebaseToken testtoken'
        self.assertEqual(1, get_user_model().objects.count())
        self.middleware(self.request)
        self.assertEqual(1, get_user_model().objects.count())
        self.assertFalse(self.request.user.is_anonymous)
        self.assertTrue(self.request.user.is_authenticated)
        user = get_user_model().objects.first()
        self.assertFalse(user.is_anonymous)
        self.assertTrue(user.is_authenticated)

    @patch(FIREBASE_VERIFY, side_effect=mock_verify)
    def test_credentials(self, verify_id_token):
        credentials = self.middleware.authenticate('whatever')
        self.assertTrue('uid' in credentials)
        self.assertTrue('name' in credentials)
        self.assertTrue('picture' in credentials)
        self.assertTrue('email' in credentials)
        self.assertEqual(4, len(credentials))
        self.assertEqual(0, get_user_model().objects.count())
