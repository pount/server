# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.handlers.asgi import ASGIHandler
from django.core.handlers.wsgi import WSGIHandler
from pount import asgi, wsgi


class ASGITest(TestCase):

    def test_asgi(self):
        self.assertEqual(ASGIHandler, type(asgi.application))


class WSGITest(TestCase):

    def test_wsgi(self):
        self.assertEqual(WSGIHandler, type(wsgi.application))
