# -*- coding: utf-8 -*-
from os.path import abspath, basename, dirname
from os import environ


######################
# Path configuration #
######################

# Tip: write paths inside the project like: s.path.join(BASE_DIR, ...)
BASE_DIR = dirname(dirname(abspath(__file__)))
SITE_ROOT = dirname(BASE_DIR)
SITE_NAME = basename(BASE_DIR)


#########################
# General configuration #
#########################

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'this_is_my_key_there_are_many_like_it_but_this_one_is_mine'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Hosts/domain names that are valid for this site.
# If DEBUG = True and ALLOWED_HOSTS is left empty here, Django will behave
# as if ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]'].
# See: https://docs.djangoproject.com/fr/3.0/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

ROOT_URLCONF = '%s.urls' % SITE_NAME

WSGI_APPLICATION = '%s.wsgi.application' % SITE_NAME

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/
STATIC_URL = '/static/'


########################
# Internationalization #
########################
# See: https://docs.djangoproject.com/en/3.0/topics/i18n/
# Language for this installation.
LANGUAGE_CODE = 'en-us'
# Local timezone for this installation.
TIME_ZONE = 'UTC'
# If USE_I18N = True, Django will do some optimization regarding i18n loading.
USE_I18N = True
# If USE_L10N = True, Django will format dates, numbers and calendars
# according to the current locale.
USE_L10N = True
# If USE_TZ = True, Django will use timezone-aware datetimes.
USE_TZ = True


#################
# Authorization #
#################

AUTH_USER_MODEL = 'api.User'

AUTHENTICATION_BACKENDS = (
    'rules.permissions.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# ADMINS will be notified of 500 errors by email.
# MANAGERS will be notified of 404 errors (use IGNORABLE_404_URLS to filter).
# See: https://docs.djangoproject.com/en/3.0/howto/error-reporting/
ADMINS = [
  # ('Your Name', 'your_email@example.com'),
]
MANAGERS = ADMINS

###########
# Logging #
###########

LOGGING = {
  'version': 1,
  'disable_existing_loggers': False,
  'formatters': {
    'default': {
      'format': '%(levelname)s %(asctime)s %(name)s:%(lineno)s %(message)s',
    },
    'django.server': {
      '()': 'django.utils.log.ServerFormatter',
      'format': '[%(server_time)s] %(message)s',
    },
  },
  'filters': {
    'require_debug_false': {
      '()': 'django.utils.log.RequireDebugFalse',
    },
    'require_debug_true': {
      '()': 'django.utils.log.RequireDebugTrue',
    },
  },
  'handlers': {
    'console': {
      'level': 'INFO',
      'filters': ['require_debug_true'],
      'class': 'logging.StreamHandler',
    },
    'django.server': {
      'level': 'INFO',
      'class': 'logging.StreamHandler',
      'formatter': 'django.server',
    },
    'mail_admins': {
      'level': 'ERROR',
      'filters': ['require_debug_false'],
      'class': 'django.utils.log.AdminEmailHandler',
    },
  },
  'loggers': {
      'apps': {
          'handlers': ['console', ],
          'level': 'DEBUG' if DEBUG else 'INFO',
          'propagate': True,
      },
  },
}


#############
# Databases #
#############

# In your virtualenv, edit the activation file and set
# the environment variables defined in this file
# ex: export DEFAULT_DB_USER='postgresql'
# See: https://docs.djangoproject.com/en/3.0/ref/settings/#databases
DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': environ.get('DEFAULT_DB_NAME'),
    'USER': environ.get('DEFAULT_DB_USER'),
    'PASSWORD': environ.get('DEFAULT_DB_PASS'),
    'HOST': environ.get('DEFAULT_DB_HOST'),
    'PORT': environ.get('DEFAULT_DB_PORT'),
  }
}

# Elasticsearch host:port configuration.
ELASTICSEARCH_URL = 'localhost:9201'


###############
# Middlewares #
###############

MIDDLEWARE = [
  'django.middleware.security.SecurityMiddleware',
  'corsheaders.middleware.CorsMiddleware',
  'django.contrib.sessions.middleware.SessionMiddleware',
  'django.middleware.common.CommonMiddleware',
  'django.middleware.csrf.CsrfViewMiddleware',
  'corsheaders.middleware.CorsPostCsrfMiddleware',
  'django.contrib.auth.middleware.AuthenticationMiddleware',
  'pount.middleware.FirebaseAuthenticationMiddleware',
  'django.contrib.messages.middleware.MessageMiddleware',
  'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


################
# Applications #
################

DJANGO_APPS = [
  'django.contrib.admin',
  'django.contrib.auth',
  'django.contrib.contenttypes',
  'django.contrib.sessions',
  'django.contrib.messages',
  'django.contrib.staticfiles',
]

THIRD_PARTY_APPS = [
  'rest_framework',  # Django REST api
  'rules',  # necessary for per-object authorization
  'corsheaders',  # CORS
]

LOCAL_APPS = [
  'apps.api',
  'apps.search',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS


#############
# Templates #
#############
# Templates are not used by this project, but Django wants its config. :|
# APP_DIRS is True because rest_framework needs its page.
TEMPLATES = [{
  'BACKEND': 'django.template.backends.django.DjangoTemplates',
  'APP_DIRS': True,
  'OPTIONS': {
    'debug': DEBUG,
    'context_processors': [
      'django.contrib.auth.context_processors.auth',
      'django.contrib.messages.context_processors.messages',
    ],
  },
}]


##########
#  CORS  #
##########
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken',
    'range',
)
CORS_REPLACE_HTTPS_REFERER = True


###########################
# Pount-specific settings #
###########################
ITEM_IMPORTERS = {
  'zenodo': 'apps.api.importers.zenodo',
  'dataverse': None,  # TODO
}
