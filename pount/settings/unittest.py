# -*- coding: utf-8 -*-
from .common import *  # noqa: F401,F403
from .common import MIDDLEWARE


DEBUG = True

firebase_middleware = 'pount.middleware.FirebaseAuthenticationMiddleware'
if firebase_middleware in MIDDLEWARE:
    MIDDLEWARE.remove(firebase_middleware)
