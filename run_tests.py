# -*- coding: utf-8 -*-


from os import environ
from sys import exit
from django import setup
from django.test.runner import DiscoverRunner

environ['DJANGO_SETTINGS_MODULE'] = 'pount.settings.unittest'
setup()

runner = DiscoverRunner(pattern='test*.py', verbosity=1,
                        interactive=False, failfast=False)
failures = runner.run_tests(['apps', 'pount'])
exit(failures)
