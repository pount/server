# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name='Pount',
    version='0.1',
    packages=find_packages(),
    maintainer='Régis Witz',
)
